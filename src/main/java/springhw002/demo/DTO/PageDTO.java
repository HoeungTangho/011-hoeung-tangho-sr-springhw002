package springhw002.demo.DTO;

public class PageDTO {
    private int id;
    private String title;
    private String discription;
    private String image;

    public PageDTO(int id, String title, String discription, String image) {
        this.id = id;
        this.title = title;
        this.discription = discription;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
