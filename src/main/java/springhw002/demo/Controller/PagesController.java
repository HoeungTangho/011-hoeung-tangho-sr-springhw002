package springhw002.demo.Controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import springhw002.demo.DTO.PageDTO;
import springhw002.demo.Service.Serviceimp.Serviceimp;

import java.util.ArrayList;

@Controller
public class PagesController {

    @Autowired
    Serviceimp serviceimp;

    public PagesController(Serviceimp serviceimp){
        this.serviceimp=serviceimp;
    }
    @GetMapping({"/","/index"})
    public String getindex(Model model){
        ArrayList<PageDTO> object=serviceimp.getAll();
        model.addAttribute("object",object);
        return "index";
    }
    @GetMapping("/view/{id}")
    public String getview(Model model, @PathVariable("id") Integer id,PageDTO pageDTO){
        pageDTO=serviceimp.view(id);
        model.addAttribute("view",pageDTO);
        return "view";
    }
     @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        if(serviceimp.delete(id)){
            System.out.println("delete success !");
        }else
            System.out.println("delete faild !");
        return "redirect:/index";
     }

}
