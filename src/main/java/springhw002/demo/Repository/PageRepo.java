package springhw002.demo.Repository;

import org.springframework.stereotype.Repository;
import springhw002.demo.DTO.PageDTO;

import java.util.ArrayList;

@Repository
public class PageRepo {
    private ArrayList <PageDTO> arrayList = new ArrayList<>();

    public PageRepo(){
        arrayList.add(new PageDTO(1, "title1", "book1", "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Gutenberg_Bible%2C_Lenox_Copy%2C_New_York_Public_Library%2C_2009._Pic_01.jpg/671px-Gutenberg_Bible%2C_Lenox_Copy%2C_New_York_Public_Library%2C_2009._Pic_01.jpg"));
        arrayList.add(new PageDTO(2, "title2", "book2", "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Gutenberg_Bible%2C_Lenox_Copy%2C_New_York_Public_Library%2C_2009._Pic_01.jpg/671px-Gutenberg_Bible%2C_Lenox_Copy%2C_New_York_Public_Library%2C_2009._Pic_01.jpg"));
        arrayList.add(new PageDTO(3, "title3", "book3", "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Gutenberg_Bible%2C_Lenox_Copy%2C_New_York_Public_Library%2C_2009._Pic_01.jpg/671px-Gutenberg_Bible%2C_Lenox_Copy%2C_New_York_Public_Library%2C_2009._Pic_01.jpg"));

    }

    public ArrayList<PageDTO> getAll(){
        return arrayList;
    }

    public PageDTO view(int id){
        for(PageDTO tmp: arrayList) {
            if (tmp.getId() == id) {
                return tmp;
            }
        }
            return null;
    }
    public boolean delete(int id){
        for(PageDTO tmp:arrayList){
            if(tmp.getId()==id){
                arrayList.remove(tmp);
                return true;
            }
        }
        return false;
    }
}
