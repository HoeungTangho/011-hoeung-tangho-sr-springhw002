package springhw002.demo.Service.Serviceimp;

import org.springframework.beans.factory.annotation.Autowired;
import springhw002.demo.DTO.PageDTO;
import springhw002.demo.Repository.PageRepo;
import springhw002.demo.Service.Service;

import java.util.ArrayList;
@org.springframework.stereotype.Service
public class Serviceimp implements Service {
    @Autowired
    PageRepo articleRepository;
    public Serviceimp(PageRepo articleRepository){
        this.articleRepository=articleRepository;

    }
    @Override
    public ArrayList<PageDTO> getAll() {
        return articleRepository.getAll();
    }

    @Override
    public PageDTO view(int id) {
        return articleRepository.view(id);
    }

    @Override
    public boolean delete(int id) {
        return articleRepository.delete(id);
    }
}
