package springhw002.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import springhw002.demo.DTO.PageDTO;
import springhw002.demo.Repository.PageRepo;
import springhw002.demo.Service.Serviceimp.Serviceimp;

import java.util.ArrayList;

public interface Service {
    ArrayList<PageDTO> getAll();
    PageDTO view(int id);
    boolean delete(int id);
}
